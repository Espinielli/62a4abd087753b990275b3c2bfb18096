Done: 

* isolate one country, i.e. Italy
* get its centroid

Todo:

* rotate it around its centroid, i.e. 20 degrees
* get axis aligned bounding box
* get area of BB
* iterate the previous for 1 degree angle in order to find the angle, alpha, of the minimum area BB
* use alpha for rotating the flags in Boetti's map


forked from <a href='http://bl.ocks.org/mbostock/'>mbostock</a>'s block: <a href='http://bl.ocks.org/mbostock/1ab623030a9d563271f55c5c55dbaf77'>Adams</a>